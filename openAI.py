import requests
import json
# ChatGPT API endpoint
#le decimos al programa si estamos en modo desarrollo y necesitamos que vaya rápido
DEV_MODE = True
#contenidos caché
#content_json_cache = #pedimos un resultado y lo guardamos

openai_key="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" #clave cuentra para entrar
api_endpoint = "https://api.openai.com/v1/chat/completions" #nos da error 401, significa que no tenemos permiso
# Symptoms list
symptoms = ["headache", "fever", "cough", "fatigue", "loss of appetite"]
# Generate query for ChatGPT API
query = "What are possible diseases that cause the following symptoms: " + ", ".join(symptoms) + "?"
# Send query to ChatGPT API
data = {
    "model": "gpt-3.5-turbo",
    "messages": [
        {
            "role": "user",
            "content": f"${query} Provide the list in JSON format"
        }
    ]
} #formato en que se deben hacer las requests

#esto es lo que es lento, si no estamos en modo depuración lo hacemos de verdad
#if not DEV_MODE:
response = requests.post(api_endpoint,
                         headers={"Authorization" : f"Bearer {openai_key}"},
                         json=data)

#pero si sí estamos en modo depuraciçon pedimos que coja la respuesta de la cahé
# else:
#     response = content_json_cache


# Get answer from ChatGPT API
response_json=response.json()

diseases = json.loads(response_json["choices"][0]["message"]["content"])

lista_enfs = list([diseases["diseases"][i]["name"] for i in range(len(diseases["diseases"]))])
# Print answer
print(lista_enfs)

#ahora preguntamos sobre medicamentos

query2 = "What medicines are used to treat the following diseases: " + ", ".join(lista_enfs) + "?"

data2 = {
    "model": "gpt-3.5-turbo",
    "messages": [
        {
            "role": "user",
            "content": f"${query2} Provide the list in JSON format"
        }
    ]
}

response2 = requests.post(api_endpoint,
                         headers = {"Authorization" : f"Bearer {openai_key}"},
                         json = data2)


response_json2 = response2.json()

medicacion = json.loads(response_json2["choices"][0]["message"]["content"])

#diccionario_enf = list(medicacion.keys())


print(medicacion)