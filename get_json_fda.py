import http.client
import requests
import json

server_url = "api.fda.gov"
drug_resource_path = "/drug/event.json?search=receivedate:[20040101+TO+20081231]&limit=1"
url = "https://api.fda.gov/drug/event.json?search=receivedate:[20040101+TO+20081231]&limit=1"

connection = http.client.HTTPSConnection(server_url)
connection.request("GET", drug_resource_path)
response = connection.getresponse()

print(response.getheaders())

data=requests.get(url) #una forma más sencilla de trabaja obteniendo datos es con esta librería
print(data.content) #obtener body de la respuesta

#convertimos de nuevo a python para manejar fácil

python_data=json.loads(data.content)

print(python_data)
print(python_data["meta"]["terms"]) #metiéndonos a sacar datos del diccionario

edad_paciente=python_data["results"][0]["patient"]["patientonsetage"]
medicina_admin=python_data["results"][0]["sender"]


print(edad_paciente)
print(medicina_admin)
connection.close()